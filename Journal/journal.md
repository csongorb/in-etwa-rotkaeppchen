# Research And Process Journal (Rotkeappchen)

## Research / Artist Statement

One of my oldest ideas, finally in the making (hopefully, somehow). It even goes back to... forever. It slightly changes all the time as technology changes, but the core is the same.

It is doable (and it should be done) with almost any fairytale, but for me it is always Rotkaeppchen. The idea is to have a playful interaction with the original fairytale, it is not a retelling. The initial idea is interaction-wise very children book-like, not necessarily a child (or anybody) alone, but with a parent. Retelling and playfully messing around with the fairytale and its elements.

In the core it is just an interactive/ digital book. Each page of the book is one location of the fairytale, page one is the first location, page two the second location, etc. Each location has an entry/ exit on each side, where characters can go to the other page/ location. The more the story of the fairytale is also moving from location to location, the better (thats the other reason Rotkaeppchen is such a perfect fit).

The main interaction for the player is to move the characters around and to turn pages. Some more additional interactions are possible (interacting with important objects, interacting with the scenery to have a living page, etc.).

An important element is text. Each page has some text, telling the story of the original fairytale as close as possible. Or rather giving some indirect clues and inspiration for a more free and playful telling of the changed story.

The main-idea is very analog book inspired. To an extent, where I also just don't know, how much of the analog-book-feeling I have to imitate to keep the idea interesting. Yes, it is an obvious idea to just make this on an endless/ scrollable canvas from left to right, but I'm sure, that it would loose something. Will have to test, find a middle ground.

## Final Research Statement / Process Reflection

### 2024-08-15b

![commit messages](20240815_git.png)

![evolution](20240815_evolution.png)

### 2024-08-15

Final? It's never final, but growing forever. But I need to close this somehow. Is this already a first reflection?

Looking back I strongly feel every possible reflection is already in the Journal, I don't really think end-reflections are needed. Maybe a short summary of all the aspects?

- finishing vs. growing
- finishing is hard
- what has happened to the book metaphor, how did it change?
- how are playfulness and narrative accompanying each other?
- working with children, motivating, keeping the motivation
- children learning game dev
- how is the idea rooted in my personal history?
- does the core idea work? (yes)
- could this be grown into something bigger? (yes)
- how does the Google Play Store work?

Anything else?

## Process Journal

### 2024-08-19

Ok, this is fantastic, fantastically absurd. Just a quick summary:

Google asked me to update the target Android API to 34. I did, but the warning (in the Google Play Console) did not go away. So I also updated the minimum Android API to 34. Still the warning did not go away. I have seen that I still have an old version of the app in the "Open testing" track. I disabled (paused) the track. The warning is still there. The "Open testing" track cannot be deleted. Finally: Updated the version in the testing track to a new one (with at least Android API 34) and the warning is gone!

No clear documentation on any of this. Some discussions and tips, mostly on StackOverflow, but no clear guidance. 

Wonderful. To be safe: will leave it as it is now.

### 2024-08-14

Done!?

Waiting for the Play Store to publish the app. I hope it works. Maybe a trailer. Final reflections? Exhibiting the game at the next Talk & Play in Berlin? But that should be it.

### 2024-08-02

Ah, a sad design sloppines, it could have been so easy: a better and more clear definition of the conditions for a text. Clear match and no-match conditions, clear none conditions, etc. But back then I have done everything fast and rough and have developed only the features that were needed in the moment... and now it is too late. On the one hand, because I want to finish. And on the other hand, because changing the condition would (possibly) also mean to change all the json-files (and I'm too lazy for that). So I will just keep now everything how it already is. But this was a design mistake for sure. A big and sloppy one.

### 2024-07-29

Just not to forget (and not to deny later): Especially now that we're finishing, it's also about the balance to keep Dalma and Cosima motivated. Writing the texts was great, great dynamics, they were both very involved and had great ideas. But in the moment where we are getting into logic (testing ourselves into the bug that caused the wrong character check for displaying the text messages), it is hard to keep them interested. Hard logic. And it has to be solved, otherwise it doesn't work. It's a delicate balance.

### 2024-07-22

In a way: haha. Let's try to finish this. It's summer and I have a few days with Dalma and Cosima alone. We'll see, but to be honest: I'm not really sure if it will work out.

What I'm thinking of since a while: With all my games-as-plants and games-are-forever-growing ideas, why do I want to finish this? What does finish mean? Is it not just ok the way it already is? It has fulfilled its purpose, it was a great experience to develop and to play around with (both during its development, but also as as a toy). But still, there is a gap in our feeling, as something would be not closed, something is still unfinished. I will have to think about this more.

Also an interesting aspect: I had to update the Androind API level for the Google Play store. This is a quite known thing, but still: is this part of the growing? In which way(s)?

### 2021-02-18

Looong break again, let's try some finishing. My semester is over, it's still lockdown, both children are at home, homeshooling (ok, Cosima will start school again next week, but only half-time).

Cosima and Dalma would enjoy to have a proper game in some kind of an app store and as they are preferring mobile, that can't be itch.io, but must be Google Play. So yeah, had to write a Privacy Policy. Frightening to realise how much user data is collected, even if I don't really want it (Unity, Android, itch.io, Google).

### 2020-11-23

A thought I have in my mind since a few days: Is this a good project to finish after a few years (around 5) without any proper game development on my own? Yes / maybe. But why?

### 2020-11-12b

How does the meaning (whatever that is in a game) of the fairytale change, that it is not set in stone, but that I can play with it, that I can mess around with its constellations? What if...?

### 2020-11-12a

A few considerations I have in my mind since a while:

By accident I have stumbled upon a book I liked as a child: [Marék Veronika: Jó jéték a papir, a filc](https://mandadb.hu/tetel/385906/Jo_jatek_a_papir__a_filc). The book if full of ideas for puppet-making and on how to play with them. I had no clue, that Marék Veronika was also just an illustrator, but also was a puppeteer, making puppets and writing stories.  
Since then I'm thinking about how this project could be connected to all this. See my notes on my inspirations below for some more detailed ideas.

Diversity and #meetoo and (even developing this with) daughters and Rotkappchen. A (the) classic fairytale (about sexuality?) clashing with my approach of raising / educating my daughters. Yes, I would like them to f*ck patriarchy and all its manifestations. Or is this the wrong terminolgy? Maybe I just want them to not even know it. But yes, Rotkaeppchen. It still fascinates me, deep inside. How to approach the fairytale in a more up-to-date way? Am I missing something?

How this step by step went away from being a book into being a dollhouse. It just found its natural form... and that is not of a book. The location-idea is just stronger. I'll have to go with it, but I'm not sure what the end-result will be. Am I doing it this game still page-turns? Or is it something else? And if not: how is everything connected to the text-elements I'm displaying?

### 2020-11-05

Finally! I had an UE-free day yesterday and Cosima stayed home and suddenly: we again have worked on the game together. That was really nice to record some sounds and put them into the game and also add some additional elements (trees, bushes, etc.). Even Dalma joined in the afternoon.

Yeah, this time the working-together was strongly pushed by me, at least in the beginning. But in the end, they had a lot of cool ideas of new graphics and other elements we still could add. So looking forward to finishing everything.

### 2020-11-02

Oh, frightening / interesting to see how much Unity & C# I learn from just one project. How much better I get with basically every new feature. Started to use some co-routines today. Should learn more about them.

### 2020-11-01

This all feels strange / stupid. I have dealt today with things that I didn't wanted to work on at all. Adding some sprite sorting logic, though Unity should be able to deal with this through its 3D logic. But it does not, as 2D sprites are rendered in a special layer, overwriting 3D. Whatever.

Tested to build some 3D scenes (cardboard-like, as suggested by Stamm and others, see below) out of the pictures of Dalma and Cosima. The possibilities are quite limited through the above mentioned constraints. Should I add all sprites as planes, so they can be part of the 3D scene? But the sprite animations are only possible with sprites, so...

Let's see how I can escape this.

### 2020-10-30

Playing around with the state of the things and procrastinating small adjustments I'm asking myself more and more if it is really important for Rotkappchen (and thus for every character) to be able to carry two items (basket and flowers) at the same time. I'm not so sure anymore. It already plays out nicely as it is now. Being able to pick up a flower with Rotkappchen and seeing her accidentally drop the basket has some nice implications. And as she can carry the flower to Grandmothers house and could realise there, that she has forgotten the basket is a nice variation. Will think about it.

Also: the "hallo" sounds are getting on my nerves already.

I'm not sure how much this behaviour has to do something with the fact, that I know the play so well, but I'm not at all looking at the texts anymore. Thats a shame, especially if we think about the original idea of this project. I'll have to playtest this a lot, for sure.

### 2020-10-29

Finally, did some Rotkaeppchen work again. Unfortunately mainly without the children. I'm showing them the results and asking them some questions (how should that sound be implemented? how can Rotkaeppchen be able to drag the Basket and some Flowers at the same time?), but now that the school has started, we have basically not time for such things together. Waiting for the lockdown. No, not really.

Nice to see how things are slowly coming together. The main interactions of the story are basically all implemented now with some general rules, that are not just there for the one particular interactions, but can also be used to mess around with the world. The only big feature still missing is that Rotkaeppchen can take the Basket and some Flowers at the same time. And finally the finishing with the children. Looking forward to the last steps.

### 2020-08-16

Started to implement that the Wolf is able to eat other characters. Awesome to see, how many interesting design questions this small feature is already raising. I love this.

Not just the more technical questions (what should happen with the item in the hand of the eaten character? how should the eaten characters escape again?), but the ones connected to the main idea of the game, to the narrative: in what situations should the Wolf be allowed to eat a character? Always? Just when they are alone (no third person present to watch)? Just in closed rooms (this would be the one following the original fairytale the most)? I would like to set up some rules that are allowing for experimentation, but also in a logical way are following the original.

What are the rules present in the fairytale? I have the strong feeling, that the rules there are not really logical. Maybe I will have to ask Cosima and Dalma. No, not maybe. Sure.

### 2020-08-04

A long long break. Don't really want to talk about the reasons, but basically: too much work. This is not exactly what I was hoping for (to put it mildly).

I hope I can manage to gain some traction again. Not just me, but the kids and me. I very much would like to finish this. It started as so much fun and now even thinking about finishing feels like work, something I have to do. I hope I can change this.

Also: for sure this was a Corona project back then. Me and Dalma and Cosima sitting together a lot. Me supervising student-projects from home. It was a perfect match. Now everything is different / normal again. A few days of holiday after an exhausting semester, finally slowly coming down, happy to have time to read a book. Cosima and Dalma starting school again in a few days. Let's see.

### 2020-06-21

Still learning git. Did managed to rearrange the repository in an ok way after students have directly commited their mods to develop and master. But hey, I sure did longer than I hoped for.

### 2020-06-19

What to do with this issue that I don't want to allow the player to drag item directly from one page to the other... BUT it would be cool, if she could drop objects to the ground (and pick them up again to give it to characters).

### 2020-05-31

Despite me being enthusiastic, this week I have just managed to work on the game for less than a hour, that's a shame. Especially as I still have to implement some more features (the special interactions between the characters, etc.) before I can start to finalise the content (texts, art, etc.). But hey, the general idea is working and proven, so what!?

### 2020-06-13

I have asked the students of the Buld a toy (first) class to mod each others games as a week-exercise. Some have asked if they can mod Rotkaeppchen and I just couldn't resist to say yes. <3

Hanna Borbola has added some art to the game, changing the forest into a much more living space. Awesomely in the style of Dalma and Cosima. Thanks!

Interesting to see that they have all added some non-Rotkaeppchen content. I don't really think that I really want this (as I strongly believe, that having a well known narrative in the core is important), but it is an obvious hint that I need more (much more) additions, secrets, things not directly related to the original fairytale.

Two students have also added some more direct (and less book-like) text affecting interactions: the player is doing something on a page and there are some direct changes in the text (like observing an object, etc). Quite obvious and game-like interactions, interesting on a lot of levels to see students implementing them. But I don't really like / want them, as they are too diverting from the core.

So yeah, this exercise was important, it has shown me what I don't really want and with this has helped me to get a better feeling for the core. Or is this just too arrogant and Rotkaeppchen has no core and students doing so much unrelated stuff is a hint I should take more seriously? Or talk with them more about how to do something FOR a game (and about how to use git, as they have directly worked on master and develop and have overwritten each others stuff)?

### 2020-05-28

Again, playtesting at the university, showed to game to some students and also to (Sebastian) Stamm and Lena (Falkenhagen). Some good observations and great feedback and awesome ideas.

The same issue again and again: where is the overlapping-area between the pages?

Art. Stamm and also the students liked the art from Dalma and Cosima and also generally the approach to integrate their ideas. Stamm said, we should use the 3d space more, not just as if it would be a drawn thing, but like a 3d space build with paper. Seems logical. Luisa (Fernanda Enciso Ulloa) had a great idea: don't just let Cosima and Dalma draw images, but let them build the 3d spaces in a paper box or something. Thanks, will try for sure!

Everybody seeking for the obvious next interactions: the wolf being able to eat other characters, etc.

### 2020-05-25

Finally, I have managed to overhaul the text-dependency system. Is it too complex now? Maybe. But I'm looking forward to write some texts for the game now, so that a good thing already.

### 2020-05-24

Added some animations. Strange, I was highly looking forward to this step, as I thought the dragging the characters around would be much more pleasing after this step... I'm not so sure anymore. Whatever. Animating the drawings of the children was great fun. I sure want to do more of this!

Some addition from a few days later:

The thing is, the characters are even feeling more static right now, than before. It's strange, because on the surface its to opposite, but hey, that's the difference between the visuals and how they feel in the game.

### 2020-05-16

Awesome to see Cosima and Dalma gaining some Technical Art knowledge, step by step. After seeing their first images in the game, they have highly adjusted they approach. Their images from yesterday are amazing, already working sprite-sheets, breaking down objects in their sub-parts. Hearing them talk and about interactions we could add to the game is pure delight. The perfect way to learn Technical Art.

I should start with animations and particle effects asap. :-)

### 2020-05-15b

Yeah, Dalma and Cosima got really motivated today and have drawn a lot a new images. Looking forward to put them into the game!

### 2020-05-15

I have added the possibility for characters to have an item in their hands and some issues appeared:

Made a funny mistake. I went for simplicity: one item per character. But the story needs at least two items in the same time in the hand of one character: Rotkaeppchen has to be able to carry the basket and some flowers to the Grandmother. There are sure some workarounds, but I'm wondering about myself that it happened.

What are the essential story-items? The basket for sure, also the flowers. But all the other ones are already questionable: gun, stones, scissor, sewing kit. And there even could be some additional ones: apples, etc.

How to approach the items systemically? I would like to allow as many combinations as possible, thats the idea. But is it ok if Rotkaeppchen can hold the gun?

Should the player be able to put an item on the ground? If yes, that also means the player can drag the items by herself from one page to the next. As this is not really a story-interaction (can items fly?), I don't think this should be allowed.

### 2020-05-13

Two seemingly small but interesting design problems:

The transition between the pages. I knew this form the beginning, but now it is getting bigger and bigger every time somebody plays the game. I need some constraint at the edges and from my POV having pages would help. But the player also have to get the idea, that this area is there as a transition between the pages. Will have to mess around with this more.

How to have enough text on each page to inspire the imagination? But how to avoid too much text?

Also:

Now that I have Android running and have also showed it there to Dalma and Cosima, it is awesome to see how they are getting the idea of how they have to redo the images for possible interactions. Dalma has drawn Rotkäppchens hause in the first version with all elements in one picture. I tried to argue with her, that she should draw the plant extra, but it has not really worked out. But now. She even had some awesome ideas for possible interactions, I'm looking forward to work with her togeter!

### 2020-05-11

First Android test. Works fine, the general interaction ideas are working well (besides the already known problems). But: the responsiveness issue is even more apparent. How responsive does this have to be? On the pages themselves? Regarding the story, the texts?

### 2020-05-10

I'm starting to realize the design wise (most) important entity / object of the game: the locations. I have to emphasize them more, make them as self-contained as possible, to be flexible. During a run I also remembered my old idea to not just play with a fairytale, but with fairytales. What happens if I mix *Rotkaeppchen* with *Haensel und Greatel*? What happens if Rotkaeppchen meets the Witch (that all frighteningly reminds me on a lot of my older texts)?

### 2020-04-30

Dalma has gotten some new inspiration to draw some new images. I finally also had some time to scan them, so here they are. We have also briefly brainstormed together how we could build the sequence of the locations and where the well and the hunter should be and also how to make the backgrounds. I did a lot of the visualisations, but it was an awesome talk. Strange, but it feels like a nice longterm cooperation, chatting casually about the possibilities.

![grandma](20200430_grandma.jpg)

![locations](20200430_locations.jpg)

![backgrounds](20200430_backgrounds.jpg)

### 2020-04-29

That was fun. I had the idea to share the prototypes with my students, to develop it during the semester next to them. I have two project modules in parallel, Build a toy (first) and P2 / Narratives, that just seems to be a natural fit. It's like two things in parallel, finally doing something for me, but also being an example (yes, this extensive journal is a part of this). Introducing the prototype worked well and they were nice (though obviously I don't exactly know how honest). Let's see if I can manage to involve them later into some more indepth design questions.

Some observations, thoughts, and conclusions:

- again, I have seen people dragging the characters outside the frame, because they didn't got, that there is an overlapping between the screens (I should maybe constrain the dragging and add some visual elements to emphasize the overlapping between the frame)
- playing in windowed mode all players have used the keyboard for left & right page turning, but none of them have realized in the first few seconds, that they can interact with the characters (as the dragging just works with mouse, and hopefully later with touch, maybe I should take out the keyboard interactions)
- some students have talked about "having several endings". That was interesting to see, as I have never thought about this experiment in this way. No endings. Just interactions. It's a playground. You can always continue playing. But you can never save. If you open the book, you always start with the first page and Rotkaeppchen and the mother
- somebody has also brought up the question of "how to take care of all the possibilities trough the texts?" and also connected to this indirectly the question of triggers. I don't really think that the solution is to say everything in the texts. The story has to be played in the mind (or through a parent playing together with a child, as stated in the first idea). The texts are just triggers. It basically should be enough to have (this will be an interesting game writing exercise) to have texts, that can fit to all situations without the need for triggers
- they liked a lot the book-like interactions and the book-like general impression, yeah!

As I just simply love (them for) how they reacted today, so here some anonymized (and shortened) notes:

- nice click and drag movement, English bitte
- the game seems like a sweet idea to create with children and to play with them
- responsive controls, emotional personal connection to game, expandability is great. Secret interactions please, that you actively search for to incentivise playthroughs. More fairytales please. Essentially the better version of a popout book
- really nice and (for us) in a good way explaining toy idea, I'm sure that kids would love this stuff
- sandbox by its very nature, implies high levels of emergent and imaginative gameplay. And it works best as a toy
- cute little story with simple straight forward interactions. It plays out like a children playground, very fitting
- a lot of potential for different endings and combinations with the interactable agents

### 2020-04-27

Aaaargh, that last step (implementing the show-text-on-page-depending-on-the-characters-on-page) was much tougher, than expected... and desired. Me having to sit in front of the computer and just doing some code without any bigger and visible change killed some of the cool dynamics I had with Cosima and Dalma. I hope I can get them back to drawing and having fun with the project asap (Dalma has already drawn some pictures I had no time to implement).

But the new and working text logic is fun already with the placeholder texts, so I'm really looking forward to assemble this into something more complete. I mean, hey, the main logic is done NOW, yeah!

Strange how Unity has gotten me. I have not worked in Unity since a while, had to relearn some of the logic (components, Game Objects, private and public, Inspector, ah!) for this last feature.

### 2020-04-26

It is coming together, thats good to see. The main interactions are already inside, that was quite fast. I think the only main logic still missing is the text I would like to have displayed on each page.

It was already fun to put Cosimas Rotkaeppchen into the project. Seeing Cosima and Dalma interact with what is already there is quite fun. I have the impression the general idea is working. Cosima has already reenacted parts of the fairytale several times and messed around with not exactly real events. I'm looking forward to fill the whole scene with their images.

The endless vs. static (digital vs. analog?) issue gets more and more visible. The project is from its core both, an endless continuous location and the static idea of the pages. Cosima was yesterday messing a lot with the overlapping of the pages, putting a character in a way to the side of the page, that it is just barely visible on the other page... thats not really something I want to have. I will have to define clear areas where the characters can go over to the next page a/o constrain the areas, where the player can put them on each page.

### 2020-04-24

Yeah, this first experiments are already proving, that it will be hard to find the middle ground between the feeling of a book and the idea of adjacent (endless scrolling) locations. Will test this further.

## Related Work

Some Rotkaeppchen related works:

- general / historical information
	- https://de.wikipedia.org/wiki/Rotk%C3%A4ppchen
	- https://en.wikipedia.org/wiki/Little_Red_Riding_Hood
- text / pictures
	- http://www.goethezeitportal.de/wissen/illustrationen/legenden-maerchen-und-sagenmotive/rotkaeppchen.html
	- http://www.maerchenlexikon.de/at-lexikon/at333.htm
	- http://www.goethe.de/lrn/prj/mlg/mad/gri/de9114344.htm
- meaning / analysis
	- https://www.zeit.de/1984/52/die-affaere-rotkaeppchen
	- Bruno Bettelheim
		- https://en.wikipedia.org/wiki/The_Uses_of_Enchantment
	- Erich Fromm
		- https://books.google.de/books/about/M%C3%A4rchen_Mythen_Tr%C3%A4ume_Eine_Einf%C3%BChrung.html?id=SKCqCgAAQBAJ
	- Verana Kast
		- https://de.wikipedia.org/wiki/Verena_Kast
		- Märchen als Therapie
- puppeteering / arts craft in Hungary (80ies?)
	- Marék Veronika
		- https://mandadb.hu/tetel/385906/Jo_jatek_a_papir__a_filc
		- https://mandadb.hu/tetel/488256/Brekkeno_es_a_Tobbiek_babjatekok
	- https://mandadb.hu/tetel/411817/A_manok_ajandeka
	- the Mühelytitkok (studio secrets) series
		- https://mandadb.hu/tetel/418628/A_babjatek
		- https://mandadb.hu/tetel/486075/Kollazs_es_montazs

### Inspirations

#### Personal

The inspirations are really hard to sort out, as this project is just so deeply me.

Why am I since forever coming back to Grimm's tales and especially Rotkaeppchen? No clue. Maybe it would be worth exploring.

The playful approach of the location based structure of the books pages are for sure inspired by Gizis old idea of the child-sized play-books. Maybe I should check them out for further inspiration.  
During the process I have encountered some even in a way deeper connections to my childhood, on a personal level, but also regarding the social context of Hungary / Budapest in the 80ies. There seems to be this strong idea of self-made puppet-making, a combination of Hungarian folk art and culture and maybe an escape from socialist structures by hand-made toys and things.  
A good example of this for sure is [Marék Veronika: Jó jéték a papir, a filc](https://mandadb.hu/tetel/385906/Jo_jatek_a_papir__a_filc), but there are also other examples. And Gizi is for sure also a result of this context, and with it also the idea of the play-books I'm mentioning above.  
I also somehow deeply remember that my parents were selling self-made puppets at some Hungarian art markets when I was very very little. But the memory is so old, maybe its not even true. I will ask them.

I'm also sure, that the idea of the [infinite canvas](https://en.wikipedia.org/wiki/Infinite_canvas), as coined and used by Scott McCloud, was an indirect huge inspiration on this.

#### Art / Style

- https://en.wikipedia.org/wiki/Paper_Mario
- https://en.wikipedia.org/wiki/Tearaway_(video_game)

## Resources / Help

- https://stackoverflow.com/questions/28983458/how-to-split-a-branch-in-two-with-git
- most sounds from: https://freesound.org/
- icons generated with: https://icon.kitchen/
