using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aspectRatioDependentPosition : MonoBehaviour
{

	public bool isActive = false;

	float startAR = (float)16 / 9; // "default" Aspect Ratio
	float endAR = (float)19.5 / 9; // Samsung A54
	float myAR = (float)Screen.width / Screen.height;

	public Vector3 myDirection;

    // Start is called before the first frame update
    void Start()
    {
    	//Debug.Log ("Screen Width : " + Screen.width);
    	//Debug.Log ("Screen Height : " + Screen.height);
    	//Debug.Log ("Start Aspect Ratio : " + startAR);
    	//Debug.Log ("End Aspect Ratio : " + endAR);
    	//Debug.Log ("My Aspect Ratio : " + myAR);

    	//Debug.Log ("==========");

    	//Debug.Log ("Total AR Distance : " + (endAR - startAR));
    	//Debug.Log ("My AR Distance : " + (myAR - startAR));

		if (isActive)
		{
			if (myAR > startAR)
			{
				//Debug.Log ("Repositioning needed!");
				float myRepositioningFactor = (float)((myAR - startAR) / (endAR - startAR));
				//Debug.Log ("Repositioning factor : " + myRepositioningFactor);

				Vector3 myNewPosition = Vector3.Lerp(this.transform.position, this.transform.position + myDirection, myRepositioningFactor);
				this.transform.position = myNewPosition;
			}
    	}
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnDrawGizmos()
	{
		Gizmos.color = Color.blue;
		Gizmos.DrawSphere(transform.position, 0.3f);

        Gizmos.DrawRay(transform.position, myDirection);
        Gizmos.DrawSphere(transform.position + myDirection, 0.2f);	
	}
}