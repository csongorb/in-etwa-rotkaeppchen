﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class billboard : MonoBehaviour
{
	void Update()
	{
		// look at camera...
		transform.LookAt(Camera.main.transform.position, -Vector3.up);
		// then lock rotation
		transform.localEulerAngles = new Vector3 (transform.localEulerAngles.x, transform.localEulerAngles.y, 0f);
	}
}
