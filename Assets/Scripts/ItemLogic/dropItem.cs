﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dropItem : MonoBehaviour
{
	public bool iCanCut;

	private string lastOwner;

	private GameObject _char;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

	void OnMouseUp()
	{
		// if items is dropped ON A CHARACTER
		if (this.GetComponent<checkTriggerContent>().charactersHere.Count > 0)
		{
			//Debug.Log(this.name + " dropped on character: " + this.GetComponent<checkTriggerContent>().charactersHere[0]);
			_char = GameObject.Find(this.GetComponent<checkTriggerContent>().charactersHere[0]);

			// if an object that can cut is dropped on a character that can eat...
			if(iCanCut && _char.GetComponent<ICanEat>() != null){
				//Debug.Log("Object that can cut on character that can eat!");
				_char.GetComponent<ICanEat>().releaseLastEatenCharacter();

				//Debug.Log("Giving back " + this.name + " to last owner: " + lastOwner);
				_char = GameObject.Find(lastOwner);
				_char.GetComponent<itemHandling>().grabItem(this.name);
			} else
			{
				// if the target character doesn't want the item
				if (_char.GetComponent<itemHandling>().doIWant(this.name) == false)
				{
					//Debug.Log(_char + " does not want the " + this.name);
					//Debug.Log("Giving back " + this.name + " to last owner: " + lastOwner);
					_char = GameObject.Find(lastOwner);
					_char.GetComponent<itemHandling>().grabItem(this.name);
				} else
				{
					// if the target character has not item in her hand
					if (_char.GetComponent<itemHandling>().hasItem.Count == 0)
					{
						_char.GetComponent<itemHandling>().grabItem(this.name);
					} else
					{
						//Debug.Log("Item exchange needed!");
						// hat name of the item in the hand of the target
						string targetsItem = _char.GetComponent<itemHandling>().hasItem[0];
						//Debug.Log(_char.name + " has item it her hand: " + targetsItem);

						// does the original owner want the item out of the targets hands?
						if (GameObject.Find(lastOwner).GetComponent<itemHandling>().doIWant(targetsItem) == true)
						{
							// empty the hand of the target character
							_char.GetComponent<itemHandling>().releaseAllItems();
							// give both characters their new items
							_char.GetComponent<itemHandling>().grabItem(this.name);
							GameObject.Find(lastOwner).GetComponent<itemHandling>().grabItem(targetsItem);
						} else
						{
							//Debug.Log("Original owner does not want " + targetsItem);
							GameObject.Find(lastOwner).GetComponent<itemHandling>().grabItem(this.name);
						}
					}
				}
			}
		} else
		{
			//Debug.Log("Giving back " + this.name + " to last owner: " + lastOwner);
			_char = GameObject.Find(lastOwner);
			_char.GetComponent<itemHandling>().grabItem(this.name);
		}
	}

	void OnMouseDown()
	{
		// this is not working somehow...
		// i have no clue why, but somehow Unity is not "getting" that an item is on a character all the time
		// if (this.GetComponent<checkTriggerContent>().charactersHere.Count > 0)
		// {
		// 	Debug.Log(this.name + " was picked up from character " + this.GetComponent<checkTriggerContent>().charactersHere[0]);
		// 	GameObject _char = GameObject.Find(this.GetComponent<checkTriggerContent>().charactersHere[0]);
		// 	_char.GetComponent<itemHandling>().releaseAllItems();
		// }

		// so I will just take away this item from every existing character
		GameObject[] allCharacters ;
        allCharacters = GameObject.FindGameObjectsWithTag("Characters");
        foreach(GameObject aChar in allCharacters)
		{
			if(aChar.GetComponent<itemHandling>().hasItem.Count > 0)
			{
				if(aChar.GetComponent<itemHandling>().hasItem[0] == this.name)
				{
            		aChar.GetComponent<itemHandling>().releaseAllItems();
					//Debug.Log(this.name + " taken away from character " + aChar.name);
					lastOwner = aChar.name;
				}
			}
        }
	}
}
