﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class itemHandling : MonoBehaviour
{
	internal List<string> hasItem = new List<string>();

	public string startItem = "";

	public List<string> itemsIDontWant = new List<string>();

    // Start is called before the first frame update
    void Start()
    {
		if (startItem != "")
		{
			this.grabItem(startItem);
		}
    }

    // Update is called once per frame
    void Update()
    {
		if (hasItem.Count > 0)
		{
		}
    }

	void OnMouseDrag()
	{
		if (hasItem.Count > 0)
		{
			stickItemToArm();
		}
	}

	void OnMouseUp()
	{
		if (hasItem.Count > 0)
		{
			stickItemToArm();
		}
	}

	public void grabItem(string _item)
	{
		if(hasItem.Count == 0)
		{
			hasItem.Add(_item);
			stickItemToArm();
		}
	}

	public void releaseAllItems()
	{
		hasItem.Clear();
	}

	public void deactivateItemInHand()
	{
		GameObject _myItem = GameObject.Find(hasItem[0]);
		_myItem.SetActive(false);
	}

	public void activateItemInHand()
	{
		GameObject _myItem = FindInactiveObjectByName(hasItem[0]);
		_myItem.SetActive(true);
		stickItemToArm();
	}

	public void stickItemToArm()
	{
		GameObject _myItem = GameObject.Find(hasItem[0]);

		// get Hand-Object from somewhere in the hierarchy
		// create _myHand object an temporary assign another object to it (so it's not empty)
		GameObject _myHand = _myItem;
		Transform[] allChildren = GetComponentsInChildren<Transform>();
		foreach(Transform child in allChildren) {
			GameObject obj = child.gameObject;
			if (obj.name == "HandPosition")
			{
				_myHand = obj;
			}
		}

		_myItem.transform.position = _myHand.transform.position;
		Vector3 _grip = new Vector3(_myItem.transform.Find("GripPosition").gameObject.transform.localPosition.x, _myItem.transform.Find("GripPosition").gameObject.transform.localPosition.y, 0.0f);
		_myItem.transform.position -= _grip;
		if (_myItem.transform.position.y < 0){
			_myItem.transform.position = new Vector3(_myItem.transform.position.x, 0f, _myItem.transform.position.z);
		}

		//Debug.Log("My depth on the Z axis is: " + this.transform.position.z);

		if (this.transform.position.z < 0)
		{
			_myItem.GetComponent<DragAndDropMe>().changeSortingLayer("Items_Foreground");
		}
		if (this.transform.position.z == 0)
		{
			_myItem.GetComponent<DragAndDropMe>().changeSortingLayer("CharacterLayer");
		}
		if (this.transform.position.z > 0)
		{
			_myItem.GetComponent<DragAndDropMe>().changeSortingLayer("Items_Background");
		}
	}

	public bool doIWant(string _item)
	{
		if (itemsIDontWant.Count == 0)
		{
			return true;
		} else
		{
			for(int i = 0; i < itemsIDontWant.Count; i++)
			{
				if (itemsIDontWant[i] == _item)
				{
					return false;
				}
			}
		}
		return true;
	}

	// as Unity can't find inactive objects by name, here is a work-around:
	// https://stackoverflow.com/questions/44456133/find-inactive-gameobject-by-name-tag-or-layer
	GameObject FindInactiveObjectByName(string name)
	{
    	Transform[] objs = Resources.FindObjectsOfTypeAll<Transform>() as Transform[];
    	for (int i = 0; i < objs.Length; i++)
    	{
        	if (objs[i].hideFlags == HideFlags.None)
        	{
            	if (objs[i].name == name)
            	{
                	return objs[i].gameObject;
            	}
        	}
    	}
    	return null;
	}
}
