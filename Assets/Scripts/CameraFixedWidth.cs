﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFixedWidth : MonoBehaviour
{

	//Set a fixed horizontal FOV
	public float horizontalFOV = 78f;

	private Camera cam;

    // Start is called before the first frame update
    void Start()
    {
		cam = gameObject.GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
		//Camera.main.fieldOfView = horizontalFOV;
		cam.fieldOfView = calcVertivalFOV(horizontalFOV, Camera.main.aspect);
    }

	// https://answers.unity.com/questions/778670/fixed-width-relative-height-on-different-aspect-ra.html
	private float calcVertivalFOV(float hFOVInDeg, float aspectRatio)
	{
		float hFOVInRads = hFOVInDeg * Mathf.Deg2Rad;
		float vFOVInRads = 2 * Mathf.Atan(Mathf.Tan(hFOVInRads / 2) / aspectRatio);
		float vFOV = vFOVInRads * Mathf.Rad2Deg;
		return vFOV;
	}
}
