﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MoveCamera : MonoBehaviour {

	public GameObject cameraObject;
	public GameObject[] cameraPositions;

	public Button leftButton;
	public Button rightButton;

	public float cameraTransitionDuration;

	public int currentCameraIndex;

	private GameObject targetCameraPosition;

	private bool mouseButtonDown;

	// Use this for initialization
	void Start () {
		currentCameraIndex = 0;

		leftButton.onClick.AddListener(doLeft);
		rightButton.onClick.AddListener(doRight);

		//move camera to 1st position
		cameraObject.transform.position = new Vector3(cameraPositions[0].transform.position.x, cameraPositions[0].transform.position.y, cameraPositions[0].transform.position.z);

		var rotationEuler = cameraPositions[currentCameraIndex].transform.rotation.eulerAngles;   //get target's rotation
		var rotation = Quaternion.Euler(rotationEuler); //transpose values
		cameraObject.transform.rotation = rotation;

	}

	// Update is called once per frame
	void Update () {

		if (Input.GetMouseButtonDown(0)){
			//print ("Pressed");
			mouseButtonDown = true;
		} else if (Input.GetMouseButtonUp(0)){
			//print ("Released");
			mouseButtonDown = false;
		}

		if (Input.GetKeyDown(KeyCode.RightArrow))
		{
			if (!mouseButtonDown){
				doRight();
			}
		}
		if (Input.GetKeyDown(KeyCode.LeftArrow))
		{
			if (!mouseButtonDown){
				doLeft();
			}
		}
	}

	// https://answers.unity.com/questions/49542/smooth-camera-movement-between-two-targets.html
	// https://forum.unity.com/threads/moving-the-camera-smoothly.464545/
	// the first time I'm using co-routines, let's see if I get them
	IEnumerator CameraTransitionTo(Transform target, float transitionDuration)
	{
    	float t = 0.0f;
    	Vector3 startingPos = cameraObject.transform.position;
		Quaternion startingRot = cameraObject.transform.rotation;
    	while (t < 1.0f)
    	{
        	t += Time.deltaTime * (Time.timeScale/transitionDuration);
     		cameraObject.transform.position = Vector3.Lerp(startingPos, target.position, t);
			cameraObject.transform.rotation = Quaternion.Slerp(startingRot, target.rotation, t);
     		yield return 0;
		}
	}

	public void doRight()
	{
		//Debug.Log("rightButton");

		currentCameraIndex ++;
		if (currentCameraIndex < cameraPositions.Length)
		{
			targetCameraPosition = cameraPositions[currentCameraIndex];
			GetComponent<playSounds>().play("pageTurningSounds");
		}
		else
		{
			currentCameraIndex = cameraPositions.Length - 1;
		}

		updatePageTexts();

		if (currentCameraIndex == cameraPositions.Length - 1)
		{
			onlyShowTextOnFrontAndLastPage();
		}

		StartCoroutine(CameraTransitionTo(targetCameraPosition.transform, cameraTransitionDuration));
	}

	public void doLeft()
	{
		//Debug.Log("leftButton");

		currentCameraIndex --;
		if (currentCameraIndex >= 0)
		{
			targetCameraPosition = cameraPositions[currentCameraIndex];
			GetComponent<playSounds>().play("pageTurningSounds");
		}
		else
		{
			currentCameraIndex = 0;
		}

		updatePageTexts();

		if (currentCameraIndex == 0)
		{
			onlyShowTextOnFrontAndLastPage();
		}

		StartCoroutine(CameraTransitionTo(targetCameraPosition.transform, cameraTransitionDuration));
	}

	void updatePageTexts()
	{
		foreach (Transform eachChild in cameraPositions[currentCameraIndex].transform)
		{
			//Debug.Log(eachChild.name);
			if (eachChild.name == "CollisionCube")
			{
				//Debug.Log ("Child found. Mame: " + eachChild.name);
				GameObject myObject = eachChild.transform.gameObject;
				//Debug.Log ("Child-object found. Mame: " + myObject);
				myObject.GetComponent<ShowPageText>().updateAllText();
			}
		}
	}

	void onlyShowTextOnFrontAndLastPage()
	{
		for (int i=0; i < cameraPositions.Length; i++){
			foreach (Transform eachChild in cameraPositions[i].transform)
			{
				//Debug.Log(eachChild.name);
				if (eachChild.name == "CollisionCube")
				{
					//Debug.Log ("Child found. Mame: " + eachChild.name);
					GameObject myObject = eachChild.transform.gameObject;
					//Debug.Log ("Child-object found. Mame: " + myObject);

					if (i == 0 || i == cameraPositions.Length-1)
					{
						myObject.GetComponent<ShowPageText>().updateAllText();
					} else {
						myObject.GetComponent<ShowPageText>().makeTextEmpty();
					}
				}
			}
		}
	}

	public void removeCharacterFromAllTriggerListsByName(string _name)
	{
		//Debug.Log ("Now: removing eaten characters from all trigger lists.");
		//Debug.Log ("Character to remove: " + _name);

		// Check ALL possible camera trigger collision cubes.
		for (int i=0; i < cameraPositions.Length; i++)
		{
			foreach (Transform eachChild in cameraPositions[i].transform)
			{
				//Debug.Log("My children: " + eachChild.name);
				if (eachChild.name == "CollisionCube")
				{
					//Debug.Log ("Collision Cube found!");
					GameObject myObject = eachChild.transform.gameObject;
					myObject.GetComponent<checkTriggerContent>().charactersHere.Remove(_name);
				}
			}
		}
	}

	public int amountCharactersOnActivePage()
	{
		//Debug.Log("We are now on page: " + currentCameraIndex);

		foreach (Transform eachChild in cameraPositions[currentCameraIndex].transform)
		{
			//Debug.Log("My children: " + eachChild.name);
			if (eachChild.name == "CollisionCube")
			{
				//Debug.Log ("Collision Cube found!");
				GameObject myObject = eachChild.transform.gameObject;
				return myObject.GetComponent<checkTriggerContent>().charactersHere.Count;
			}
		}
		return 0;
	}
}
