// needs some other scripts on the same object to work:
// - checkTriggerContent

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ICanEat : MonoBehaviour
{
	[HideInInspector]
	public List<GameObject> charactersEaten = new List<GameObject>();
	//private List<GameObject> itemsEaten = new List<GameObject>();

	private bool dangerSoundPlayed = false;

	public whoCanIEat[] myMenu;

	Animator animator;

	[System.Serializable]
	public class whoCanIEat
	{
		public string eatableChar;
		public GameObject objectToShow;

		public whoCanIEat()
		{
		}
	}

    // Start is called before the first frame update
    void Start()
    {
		animator = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
		if (Input.GetKeyDown(KeyCode.Space))
		{
			//releaseLastEatenCharacter();
		}
    }

	/*    
	void OnMouseDown()
	{
		// Debug
		Debug.Log ("I have already eaten so many characters: " + charactersEaten.Count);
		if (charactersEaten.Count > 0)
		{
			Debug.Log ("I have already eaten the following characters:");
			for (int i=0; i < charactersEaten.Count; i++)
			{
				Debug.Log (charactersEaten[i]);
			}
		}
	}
	*/

	void OnMouseDrag()
	{

		// how many characters are here?
		GameObject _manager = GameObject.Find("Game Manager");
		//Debug.Log("The amount of characters on the active page is: " + _manager.gameObject.transform.GetComponent<MoveCamera>().amountCharactersOnActivePage());
		int amontOfCharOnActivePage = _manager.gameObject.transform.GetComponent<MoveCamera>().amountCharactersOnActivePage();

		if (canIEatThis() && charactersEaten.Count < 2 && amontOfCharOnActivePage <= 2){
			if (!dangerSoundPlayed){
				GetComponent<playSounds>().play("iWantToEatYou");
				dangerSoundPlayed = true;
				animator.SetBool("isAttacking", true);
			}
		}

		if (canIEatThis() && charactersEaten.Count >= 2)
		{
			//Debug.Log("I can't eat anymore! Nooo!");
		}

		if (this.GetComponent<checkTriggerContent>().charactersHere.Count == 0){
			dangerSoundPlayed = false;
			animator.SetBool("isAttacking", false);
		}
	}

	void OnMouseUp()
	{

		// how many characters are here?
		GameObject _manager = GameObject.Find("Game Manager");
		//Debug.Log("The amount of characters on the active page is: " + _manager.gameObject.transform.GetComponent<MoveCamera>().amountCharactersOnActivePage());
		int amontOfCharOnActivePage = _manager.gameObject.transform.GetComponent<MoveCamera>().amountCharactersOnActivePage();

		if (charactersEaten.Count < 2 && amontOfCharOnActivePage <= 2){
			if (canIEatThis()){

				// play some eating audio
				if (GetComponent<AudioSource>() != null && GetComponent<playSounds>() != null)
				{
					GetComponent<playSounds>().play("iAmEating");
				}

				// eating the character
				string _nameOfCharToEat = this.GetComponent<checkTriggerContent>().charactersHere[0];

				animator.SetBool("isAttacking", false);

				charactersEaten.Add(GameObject.Find(_nameOfCharToEat));
				GameObject _char = GameObject.Find(_nameOfCharToEat);
				_char.SetActive(false);

				// showing the object (clothing, etc.) related to the eaten character
				foreach (whoCanIEat eachChild in myMenu)
				{
					if (eachChild.eatableChar == _nameOfCharToEat){
						//Debug.Log ("I have eaten " + eachChild.eatableChar);
						// activate object
						eachChild.objectToShow.SetActive(true);
					}
				}

				// Manually removing the name of the GameObject from the Trigger-List
				// (as is just removes the objects by entering / exiting the trigger)
				this.GetComponent<checkTriggerContent>().charactersHere.Remove(_nameOfCharToEat);
				// Oh, I also have to remove the character from the trigger-list of the location
				// It's not "where am I"? No! It's: "here is the character who is gotten eaten?".
				//GameObject _manager = GameObject.Find("Game Manager");
				_manager.gameObject.transform.GetComponent<MoveCamera>().removeCharacterFromAllTriggerListsByName(_nameOfCharToEat);

				// checking for an itam in their hand
				if (_char.GetComponent<itemHandling>().hasItem.Count != 0){
					//Debug.Log("Eaten character has an item!");
					_char.GetComponent<itemHandling>().deactivateItemInHand();

					// Manually removing the name of the GameObject from the Trigger-List
					// (as is just removes the objects by entering / exiting the trigger)
					this.GetComponent<checkTriggerContent>().itemsHere.Remove(_char.GetComponent<itemHandling>().hasItem[0]);
				}
			}
		}
	}

	public void releaseLastEatenCharacter()
	{
		//Debug.Log(this.name + " has already eaten " + charactersEaten.Count + " characters.");
		if (charactersEaten.Count > 0)
		{
			//Debug.Log("Now releasing the last eaten character: " + charactersEaten[charactersEaten.Count-1].name);
			GameObject _char = charactersEaten[charactersEaten.Count-1];
			_char.transform.position = this.transform.position + new Vector3(-2, 0, 0);
			_char.SetActive(true);
			charactersEaten.RemoveAt(charactersEaten.Count - 1);

			//Debug.Log("Now releasing the last eaten character: " + _char.name);

			// remove the object related to the eaten character
			foreach (whoCanIEat eachChild in myMenu)
			{
				if (eachChild.eatableChar == _char.name){
					//Debug.Log ("I have eaten " + eachChild.eatableChar);
					// activate object
					eachChild.objectToShow.SetActive(false);
				}
			}

			// checking for an item in the hand
			if (_char.GetComponent<itemHandling>().hasItem.Count != 0){
				//Debug.Log("Eaten character has an item!");
				_char.GetComponent<itemHandling>().activateItemInHand();
			}
		}
	}

	bool canIEatThis()
	{
		if (this.GetComponent<checkTriggerContent>().charactersHere.Count > 0)
		{
			foreach (whoCanIEat eachChild in myMenu)
			{
				if (eachChild.eatableChar == this.GetComponent<checkTriggerContent>().charactersHere[0])
				{
					return true;
				}
			}
		}
		return false;
	}
}
