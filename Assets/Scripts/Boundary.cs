﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boundary : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {

    }

	void Update()
	{

	}

	void OnTriggerEnter(Collider collider)
	{
		//Debug.Log ("Something is here (at a boundary)! OnTriggerEnter");

		GameObject myObject = collider.gameObject.transform.parent.gameObject;

		bool fromLeft;

		if (myObject.transform.position.x < this.transform.position.x){
			//Debug.Log ("Object/character arriving from RIGHT.");
			fromLeft = true;
		} else {
			//Debug.Log ("Object/character arriving from LEFT.");
			fromLeft = false;
		}

		if (myObject.gameObject.tag == "Characters" || myObject.gameObject.tag == "Items")
		{
			//Debug.Log ("A character or item collided with me (a boundary).");

			float myWidth = this.GetComponent<Collider>().bounds.size.x;
			//Debug.Log ("My-width: " + myWidth);
			float objectWidth = myObject.GetComponentInChildren<Collider>().bounds.size.x;
			// this is just getting the FIRST collider of the object!
			//Debug.Log ("Object-width: " + objectWidth);

			float myDistance = (myWidth*2) + objectWidth;

			if (fromLeft)
			{
				myDistance = myDistance * (-1);
			}

			myObject.transform.position = new Vector3 (this.transform.position.x + myDistance, myObject.transform.position.y, 0f);
		}
	}
}
