﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.IO;

[System.Serializable]
public class ShowPageText : MonoBehaviour
{

	public GameObject myTextObject; // TextMeshPro object to change

	public string myJsonFilesPath;

	private DefaultTexts myDefaultTexts;

	private PageText[] myPageTexts;

	private TextMeshPro textmeshPro;

	[System.Serializable]
	public class DefaultTexts
	{
		[TextArea(10,15)]
		public string defaultText;
		[TextArea(4,6)]
		public string oneCharacter_Beginning;
		[TextArea(4,6)]
		public string oneCharacter_End;
		[TextArea(4,6)]
		public string moreCharacters_Beginning;
		[TextArea(4,6)]
		public string moreCharacters_End;
	}

	[System.Serializable]
	public class PageText
	{

		public necessaryCharactersOnAPage[] myNecessaryCharactersOnAPage;
		public necessaryItemWithACharacter[] myNecessaryItemsWithCharacters;
		public necessaryCharactersEaten[] myNecessaryCharactersEaten;

		[TextArea(8,12)]
		public string pageText;

		//[System.NonSerialized] public...

		public PageText()
		{
		}

		public bool isMatch()
		{

			if (areItemsWithCharacters() == false)
			{
				return false;
			}

			if (areAllCharactersOnThePages() == false)
			{
				return false;
			}

			if (areAllCharactersEaten() == false)
			{
				return false;
			}

			return true;
		}

		public bool areAllCharactersOnThePages()
		{

			foreach(necessaryCharactersOnAPage myCharactersOnAPage in myNecessaryCharactersOnAPage)
			{
				//Debug.Log ("Have to check the following page for some characters: " + myCharactersOnAPage.nameOfThePage);

				GameObject myPage = GameObject.Find(myCharactersOnAPage.nameOfThePage);
				//Debug.Log(myPage.name);

				for(int i = 0; i < myCharactersOnAPage.necessaryCharactersByName.Length; i++)
				{
					//Debug.Log("Is " + myCharactersOnAPage.necessaryCharactersByName[i] + " here?");

					if (myPage.GetComponentInChildren<checkTriggerContent>().charactersHere.Contains(myCharactersOnAPage.necessaryCharactersByName[i]))
					{
						//Debug.Log("Yes! S/he is here!");
					} else
					{
						//Debug.Log("No, s/he is not here.");
						return false;
					}
				}

				if (myCharactersOnAPage.otherCharactersAllowed == false)
				{
					//Debug.Log("Other characters are not allowed. Match has to be exact!");

					if (myCharactersOnAPage.necessaryCharactersByName.Length == myPage.GetComponentInChildren<checkTriggerContent>().charactersHere.Count)
					{
						//Debug.Log("Match IS exact, yeah!");
					} else
					{
						//Debug.Log("Match IS NOT exact.");
						return false;
					}
				}

				//if (myCharactersOnAPage.necessaryCharactersByName[0] == "none")
				//{
				//	Debug.Log("Checking for NONE...");
				//}
			}

			return true;
		}

		public bool areItemsWithCharacters()
		{

			if(myNecessaryItemsWithCharacters.Length == 0)
			{
			} else
			{

				foreach(necessaryItemWithACharacter myItemWithACharacter in myNecessaryItemsWithCharacters)
				{
					//Debug.Log("Does " + myItemWithACharacter.characterName + " have " + myItemWithACharacter.itemName + "?");

					GameObject myChar = GameObject.Find(myItemWithACharacter.characterName);

					if(myChar != null)
					{

						if(myChar.GetComponent<itemHandling>().hasItem.Count > 0)
						{
							//Debug.Log(myChar.GetComponent<itemHandling>().hasItem[0]);
							if (myItemWithACharacter.itemName == myChar.GetComponent<itemHandling>().hasItem[0])
							{
								//Debug.Log("Yes, s/he has.");
							} else
							{
								//Debug.Log("No, s/he has something else in the hand.");
								return false;
							}
						} else {
							//Debug.Log("No, s/he has nothing in the hand.");
							return false;
						}
					}
				}
			}
			return true;
		}

		public bool areAllCharactersEaten(){

			if(myNecessaryCharactersEaten == null)
			{
				//Debug.Log ("Can't find a condition to check, if a character has eaten anybody...");
				return true;
			} else
			{
				//Debug.Log ("Will check the conditions, if a character has eaten anybody...");
				if(myNecessaryCharactersEaten.Length == 0)
				{
					//Debug.Log ("No characters to check, if they have eaten anybody...");
					//return true;
				} else
				{
					//Debug.Log ("Will do some checks for characters, if they have eaten anybody...");
					//Debug.Log ("Count of conditions to check, if a character has eaten anybody: " + myNecessaryCharactersEaten.Length);

					foreach(necessaryCharactersEaten myHaveCharactersEatenConditons in myNecessaryCharactersEaten)
					{
						//Debug.Log ("Has " + myHaveCharactersEatenConditons.characterNameWhoCanEat + " have eaten anybody? Checking...");

						GameObject myCharToCheck = GameObject.Find(myHaveCharactersEatenConditons.characterNameWhoCanEat);

						if (myCharToCheck != null)
						{
							//Debug.Log ("Found GameObject!");
							//Debug.Log ("I have eaten " + myCharToCheck.GetComponent<ICanEat>().charactersEaten.Count + " characters.");

							// NONE
							if(myCharToCheck.GetComponent<ICanEat>().charactersEaten.Count == 0)
							{
								//Debug.Log ("I have eaten no characters!");
								if(myHaveCharactersEatenConditons.charactersEatenByName[0] == "none")
								{
									//Debug.Log ("Text to display depending on the condition, that I have eaten nobody.");
									return true;
								} else {
									return false;
								}
							}

							// ANYBODY
							if(myCharToCheck.GetComponent<ICanEat>().charactersEaten.Count > 0)
							{
								//Debug.Log ("I have eaten at least one character!");
								if(myHaveCharactersEatenConditons.charactersEatenByName[0] == "anybody")
								{
									//Debug.Log ("Text to display depending on the condition, that I have eaten somebody.");
									return true;
								} else {
									//return false;
								}
							}

							int matchesFound = 0;

							if (myCharToCheck.GetComponent<ICanEat>().charactersEaten.Count != myHaveCharactersEatenConditons.charactersEatenByName.Length)
							{
								// if the character has eaten more characters than the conditions are for...
								return false;
							}

							foreach(string eatenCharsToCeck in myHaveCharactersEatenConditons.charactersEatenByName){
								//Debug.Log ("Have I eaten " + eatenCharsToCeck + "?");

								foreach(GameObject myEatenChars in myCharToCheck.GetComponent<ICanEat>().charactersEaten)
								{
									//Debug.Log ("I have eaten: " + myEatenChars.name);
									if(myEatenChars.name == eatenCharsToCeck)
									{
										//Debug.Log ("Yes! I have eaten a character I was supposed to!");
										matchesFound++;
									}
								}
							}

							//Debug.Log ("Matches found: " + matchesFound + " (out of " + myHaveCharactersEatenConditons.charactersEatenByName.Length + ")");
							if (matchesFound == myHaveCharactersEatenConditons.charactersEatenByName.Length)
							{
								return true;
							} else {
								return false;
							}
						}
					}
				}
			}
			return false;
		}
	}

	[System.Serializable]
	public class necessaryCharactersOnAPage
	{
		public string nameOfThePage;
		public string[] necessaryCharactersByName;
		public bool otherCharactersAllowed;
	}

	[System.Serializable]
	public class necessaryItemWithACharacter
	{
		public string characterName;
		public string itemName;
	}

	[System.Serializable]
	public class necessaryCharactersEaten
	{
		public string characterNameWhoCanEat;
		public string[] charactersEatenByName;
	}

	void Awake()
    {
        textmeshPro = myTextObject.GetComponent<TextMeshPro>();
    }

    // Start is called before the first frame update
    void Start()
    {
		loadTextsFromJson();
    }

    // Update is called once per frame
    void Update()
    {
		if(Input.GetKeyDown(KeyCode.J))
		{
			//Debug.Log("J!");

			// add some temp data, to have some more full Json files as templates
			//addAndOverwriteSomeData();

			//saveTextsToJson();
		}

		if(Input.GetKeyDown(KeyCode.L))
		{
			//Debug.Log("L!");
			//loadTextsFromJson();
		}
    }

	void addAndOverwriteSomeData()
	{
		myPageTexts = new PageText[2];
		myPageTexts[0] = new PageText();
		myPageTexts[0].myNecessaryCharactersOnAPage = new necessaryCharactersOnAPage[2];
		myPageTexts[0].myNecessaryCharactersOnAPage[0] = new necessaryCharactersOnAPage();

		myPageTexts[0].myNecessaryCharactersOnAPage[0].nameOfThePage = "This Page.";
		myPageTexts[0].myNecessaryCharactersOnAPage[0].necessaryCharactersByName = new string[3];
		myPageTexts[0].myNecessaryCharactersOnAPage[0].necessaryCharactersByName[0] = "Char1";
		myPageTexts[0].myNecessaryCharactersOnAPage[0].necessaryCharactersByName[1] = "Char2";
		myPageTexts[0].myNecessaryCharactersOnAPage[0].necessaryCharactersByName[2] = "Char3";
		myPageTexts[0].myNecessaryCharactersOnAPage[0].otherCharactersAllowed = true;

		myPageTexts[0].myNecessaryCharactersOnAPage[1] = new necessaryCharactersOnAPage();
		myPageTexts[0].myNecessaryItemsWithCharacters = new necessaryItemWithACharacter[2];
		myPageTexts[0].myNecessaryItemsWithCharacters[0] = new necessaryItemWithACharacter();
		myPageTexts[0].myNecessaryItemsWithCharacters[0].characterName = "Rot";
		myPageTexts[0].myNecessaryItemsWithCharacters[0].itemName = "Blau";
		myPageTexts[0].myNecessaryItemsWithCharacters[1] = new necessaryItemWithACharacter();
		myPageTexts[0].myNecessaryItemsWithCharacters[1].characterName = "Gelb";
		myPageTexts[0].myNecessaryItemsWithCharacters[1].itemName = "Grün";
		myPageTexts[0].pageText = "Hallo Welt.";
	}

	void loadTextsFromJson()
	{
		var json_myDefaults = Resources.Load<TextAsset>(myJsonFilesPath + "default");
		var json_myOthers = Resources.Load<TextAsset>(myJsonFilesPath + "other");

		//Debug.Log(json_myDefaults);
		//Debug.Log(json_myOthers);

		myDefaultTexts = JsonUtility.FromJson<DefaultTexts>(json_myDefaults.ToString());
		myPageTexts = JsonHelper.FromJson<PageText>(json_myOthers.ToString());
	}

	void saveTextsToJson()
	{
		// attention!
		// only for working purposes!
		// has no real value in a build!

		string defaultTextsToJson = JsonUtility.ToJson(myDefaultTexts, true);
		Debug.Log(defaultTextsToJson);
		string pageTextsToJson = JsonHelper.ToJson(myPageTexts, true);
		Debug.Log(pageTextsToJson);

		string myName = transform.parent.parent.gameObject.name;
		//Debug.Log(myName);

		// saves data into the main folder of the Unity project!

		string path_to_json = myName + "_default.json";
		File.WriteAllText(path_to_json, defaultTextsToJson);

		path_to_json = myName + "_other.json";
		File.WriteAllText(path_to_json, pageTextsToJson);
	}

	public void updateAllText()
	{
		// Debug.Log("Updating Texts...");
		// Debug.Log("Checking objects: " + myPageTexts.Length);

		bool isAnyMatchTrue = false;

		textmeshPro.text = "";

		for(int i = 0; i < myPageTexts.Length; i++)
		{
			// Debug.Log(myPageTexts[i].isMatch(charactersHere));
			 if(myPageTexts[i].isMatch() == true)
			 {
			 	//Debug.Log(myPageTexts[i].pageText);
			 	textmeshPro.text = textmeshPro.text + myPageTexts[i].pageText;
			 	isAnyMatchTrue = true;
			 }
		}

		//Debug Information
		//Debug.Log("Checking default texts...");
		//Debug.Log("Number of characters: " + this.GetComponent<checkTriggerContent>().charactersHere.Count);
		//if (this.GetComponent<checkTriggerContent>().charactersHere.Count > 0)
		//{
		//	for(int i = 0; i < this.GetComponent<checkTriggerContent>().charactersHere.Count; i++)
		//	{
		//		Debug.Log ("Character Nr. " + i + " is: " + this.GetComponent<checkTriggerContent>().charactersHere[i]);
		//	}
		//}


		// if non of the character-combinations is true, BUT ONE character is here
		if (isAnyMatchTrue == false && this.GetComponent<checkTriggerContent>().charactersHere.Count == 1)
		{
			textmeshPro.text = myDefaultTexts.oneCharacter_Beginning + GameObject.Find(this.GetComponent<checkTriggerContent>().charactersHere[0]).GetComponent<ImACharacter>().myNameDE + myDefaultTexts.oneCharacter_End;
		}

		// if non of the character-combinations is true, BUT MORE CHARACTERS are here
		if (isAnyMatchTrue == false && this.GetComponent<checkTriggerContent>().charactersHere.Count > 1)
		{
			string characterNames = "";

			for(int i = 0; i < this.GetComponent<checkTriggerContent>().charactersHere.Count; i++)
			{
				if(i != 0)
				{
					if (i == this.GetComponent<checkTriggerContent>().charactersHere.Count - 1)
					{
						characterNames = string.Concat(characterNames, " und ");
					} else {
						characterNames = string.Concat(characterNames, ", ");
					}
				}

				characterNames = string.Concat(characterNames, GameObject.Find(this.GetComponent<checkTriggerContent>().charactersHere[i]).GetComponent<ImACharacter>().myNameDE);

			}
			textmeshPro.text = myDefaultTexts.moreCharacters_Beginning + characterNames + myDefaultTexts.moreCharacters_End;
		}

		// if non of the character-combinations is true, AND NO CHARACTER is here
		if (isAnyMatchTrue == false && this.GetComponent<checkTriggerContent>().charactersHere.Count == 0)
		{
			if (textmeshPro.text != null)
			{
				textmeshPro.text = myDefaultTexts.defaultText;
			} else {
				textmeshPro.text = "";
			}
		}
	}

	public void makeTextEmpty()
	{
		textmeshPro.text = "";
	}

	// ===========
	// https://stackoverflow.com/questions/36239705/serialize-and-deserialize-json-and-json-array-in-unity
	// ===========

	public static class JsonHelper
	{
    	public static T[] FromJson<T>(string json)
    	{
        	Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
        	return wrapper.Items;
    	}

    	public static string ToJson<T>(T[] array)
    	{
        	Wrapper<T> wrapper = new Wrapper<T>();
        	wrapper.Items = array;
        	return JsonUtility.ToJson(wrapper);
    	}

    	public static string ToJson<T>(T[] array, bool prettyPrint)
    	{
        	Wrapper<T> wrapper = new Wrapper<T>();
        	wrapper.Items = array;
        	return JsonUtility.ToJson(wrapper, prettyPrint);
    	}

    	[System.Serializable]
    	private class Wrapper<T>
    	{
        	public T[] Items;
    	}
	}
}
