﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateMeDoor : MonoBehaviour
{
	public Animator animator;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

	void OnMouseDown()
	{
		if (animator.GetBool("isOpen")){
			//Debug.Log ("The door is open. Closing it...");
			animator.SetBool("isOpen", false);
		} else {
			//Debug.Log ("The door is closed. Opening it...");
			animator.SetBool("isOpen", true);
		}
	}

	void OnMouseUp()
	{
		//animator.SetBool("isDragging", false);
	}
}
