﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gizmo_DrawCamera : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

	void OnDrawGizmos ()
	{
		Gizmos.color = Color.red;
		Gizmos.matrix = transform.localToWorldMatrix;           // For the rotation bug
		Gizmos.DrawFrustum(transform.position, Camera.main.fieldOfView, Camera.main.nearClipPlane, Camera.main.farClipPlane, Camera.main.aspect);
	}
}
