﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gizmo_ShowPosition : MonoBehaviour
{
	public Color gizmoColor;
	public float gizmoSize;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

	private void OnDrawGizmos()
	{
		Gizmos.color = new Color (gizmoColor.r, gizmoColor.g, gizmoColor.b);
		Gizmos.DrawSphere(transform.position, gizmoSize);
	}
}
