using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// inspired by:
// https://codyburleson.com/blog/unity-recipes-head-bob-or-breathe

public class SinusRotation : MonoBehaviour {

	Quaternion baseRot;
    public float amplitude = 5f;
    public float period = 5f;

    void Start() {
    }

    void Update() {
		baseRot = transform.parent.transform.rotation;
        float theta = Time.timeSinceLevelLoad / period;
        float distance = amplitude * Mathf.Sin(theta);

		// adding the calculated rotation to the base rotation
        transform.rotation = baseRot * Quaternion.Euler(0, 0, distance);
    }
}
