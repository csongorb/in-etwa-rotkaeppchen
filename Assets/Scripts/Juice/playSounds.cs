﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class playSounds : MonoBehaviour
{

	public SoundsContainer[] mySoundsContainers;

	[System.Serializable]
	public class SoundsContainer
	{
		public string name;
		public AudioClip[] sounds;

		public SoundsContainer()
		{
		}

	}

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

	public void play(string containerToPlay)
	{
		foreach (SoundsContainer eachChild in mySoundsContainers)
		{
			if (eachChild.name == containerToPlay){
				int playNow = Random.Range(0, eachChild.sounds.Length);
				GetComponent<AudioSource>().clip = eachChild.sounds[playNow];
				GetComponent<AudioSource>().Play();
			}
		}
	}
}
