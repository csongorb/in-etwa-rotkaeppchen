﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseRotationAround : MonoBehaviour
{
	public float rotationMultiplier = 0.5f;

	Vector3 rotateAroundPos;

	// not really working / finished
	// movement not relative to the middle of the screen

    void Start() {

    }

    void Update() {
		rotateAroundPos = new Vector3 (transform.position.x, 0f, 0f);

		transform.RotateAround(rotateAroundPos, Vector3.up, Input.GetAxis("Mouse X") * rotationMultiplier);
		transform.RotateAround(rotateAroundPos, Vector3.right, Input.GetAxis("Mouse Y") * rotationMultiplier);
    }
}
