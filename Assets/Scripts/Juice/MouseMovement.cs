﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseMovement : MonoBehaviour
{
	Vector3 basePos;
    public float amplitude = 5f;

    void Start() {
    }

    void Update() {

		// https://docs.unity3d.com/Manual/PlatformDependentCompilation.html
		#if UNITY_STANDALONE

			basePos = transform.parent.transform.position;

			//Debug.Log ("MouseX: " + Input.mousePosition.x + ", MouseY: " + Input.mousePosition.y);

			float distX = Mathf.Lerp(-amplitude, amplitude, (Input.mousePosition.x/Screen.width));
			float distY = Mathf.Lerp(-amplitude, amplitude, (Input.mousePosition.y/Screen.height));

			Vector3 distance = new Vector3 (distX, distY, 0f);

			transform.position = basePos + distance;

		#endif
    }
}
