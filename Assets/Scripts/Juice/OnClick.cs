using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class OnClick : MonoBehaviour
{
	public float scaleChangeFloat = 1.0f;

	[Header("Optional")]

	public GameObject otherGameObject;
	public float scaleChangeOtherFloat = 1.0f;

	private Vector3 scaleChange;
	private Vector3 scaleChangeOther;

    // Start is called before the first frame update
    void Start()
    {
		scaleChange = new Vector3(scaleChangeFloat, scaleChangeFloat, scaleChangeFloat);
		if (otherGameObject != null)
		{
			scaleChangeOther = new Vector3(scaleChangeOtherFloat, scaleChangeOtherFloat, scaleChangeOtherFloat);
		}
    }

    // Update is called once per frame
    void Update()
    {


    }

	void OnMouseOver()
	{
		//If your mouse hovers over the GameObject with the script attached, output this message
		//Debug.Log("Mouse is over GameObject: " + this.transform.gameObject.name);
	}

	void OnMouseDown()
	{
		// block if click is over some UI
		if (EventSystem.current.IsPointerOverGameObject()) return;
		// argh, does not work for touch, so here is a workaround
		if (IsPointerOverUIObject()) return;

		// play audio
		if (GetComponent<AudioSource>() != null && GetComponent<playSounds>() != null)
		{
			GetComponent<playSounds>().play("onClick");
		}

		// adjust scale

		this.transform.localScale += scaleChange;
		if (otherGameObject != null)
		{
			//Debug.Log ("Another object detected.");
			otherGameObject.transform.localScale += scaleChangeOther;
		}
	}

	void OnMouseDrag()
	{
	}

	void OnMouseUp()
	{
		this.transform.localScale -= scaleChange;
		if (otherGameObject != null)
		{
			otherGameObject.transform.localScale -= scaleChangeOther;
		}
	}

	void OnMouseExit()
	{
		//The mouse is no longer hovering over the GameObject so output this message each frame
		//Debug.Log("Mouse is no longer on GameObject.");
	}

	// idea stolen from: http://forum.unity3d.com/threads/ispointerovereventsystemobject-always-returns-false-on-mobile.265372/
	private bool IsPointerOverUIObject() 
	{
    	PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
    	eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
    	List<RaycastResult> results = new List<RaycastResult>();
    	EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
    	return results.Count > 0;
	}
}
