﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateMeDraggable : MonoBehaviour
{
	Animator animator;

    // Start is called before the first frame update
    void Start()
    {
		animator = gameObject.GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {

    }

	void OnMouseDown()
	{
		animator.SetBool("isDragging", true);
	}

	void OnMouseUp()
	{
		animator.SetBool("isDragging", false);
	}
}
