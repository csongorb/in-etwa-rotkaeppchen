﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class randomSpriteAtStart : MonoBehaviour
{
	private int rand;

	public Sprite[] mySprites;

    // Start is called before the first frame update
    void Start()
    {
		rand = Random.Range(0, mySprites.Length);
		GetComponent<SpriteRenderer>().sprite = mySprites[rand];
    }

    // Update is called once per frame
    void Update()
    {

    }
}
