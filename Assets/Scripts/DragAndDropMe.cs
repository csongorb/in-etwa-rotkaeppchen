﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragAndDropMe : MonoBehaviour
{
	public float scaleChangeFactor = 1.1f;

	private Vector3 myStartScale;
	private Vector3 scaleChange;

	private bool draggingDirectionIsToRight;
	private Vector3 dragPreviousPos = Vector3.zero;

	private SpriteRenderer[] mySprites;

	public bool mySpriteCanChangeDirection = false;
	public bool spriteFacingToRight = true;

	public int baseSpriteOrder = 0;

	private GameObject myGameManager;

	private GameObject myPagesPageManager;
	private GameObject myPagesCollisionBox;

    // Start is called before the first frame update
    void Start()
    {
		myStartScale = this.transform.localScale;
		scaleChange = new Vector3(scaleChangeFactor, scaleChangeFactor, 1f);

		if (spriteFacingToRight){
			draggingDirectionIsToRight = true;
		} else {
			draggingDirectionIsToRight = false;
		}

		mySprites = GetComponentsInChildren<SpriteRenderer>();

		myGameManager = GameObject.Find("Game Manager");

		myPagesCollisionBox = new GameObject();
    }

    void FixedUpdate()
    {
    	updateItemPositionInHandOfCharacter();
    }

	void OnMouseOver()
	{
		//If your mouse hovers over the GameObject with the script attached, output this message
		//Debug.Log("Mouse is over GameObject: " + this.transform.gameObject.name);
	}

	void OnMouseDown()
	{
		//Debug.Log ("You have clicked on a: " + this.tag);
		//Debug.Log ("Your active Page Index is: " + myGameManager.GetComponent<MoveCamera>().currentCameraIndex);
		//Debug.Log ("Last Page has Index: " + (myGameManager.GetComponent<MoveCamera>().cameraPositions.Length - 1));

		if (myGameManager.GetComponent<MoveCamera>().currentCameraIndex == 0 || myGameManager.GetComponent<MoveCamera>().currentCameraIndex == myGameManager.GetComponent<MoveCamera>().cameraPositions.Length - 1.0)
		{
			//Debug.Log ("You have clicked on a draggable character on the first or last page.");
		} else 
		{
			//Debug.Log ("You have clicked on a dragable character NOT on the first or last page.")
		}

		// checking on which page we are
		//Debug.Log ("You have clicked on page: " + myGameManager.GetComponent<MoveCamera>().currentCameraIndex);
		// get collision box
		myPagesPageManager = myGameManager.GetComponent<MoveCamera>().cameraPositions[myGameManager.GetComponent<MoveCamera>().currentCameraIndex];
		myPagesCollisionBox = new GameObject();
		foreach (Transform _t in myPagesPageManager.transform)
 		{
     		if(_t.name == "CollisionCube")
			{
				myPagesCollisionBox = _t.gameObject;
			}
 		}
		//Debug.Log ("Count of characters on this page: " + myPagesCollisionBox.GetComponent<checkTriggerContent>().charactersHere.Count);

		// transform items to the main line for standartised collision checking
		Ray tempRay = Camera.main.ScreenPointToRay(Input.mousePosition);
		// make two planes
		Plane tempPlaneZero = new Plane(Vector3.back, Vector3.zero);
		Plane tempPlaneObject = new Plane(Vector3.back, this.transform.position.z);
		// get point where the mouse hits the main line
		float tempDistanceToZero = 0f;
		tempPlaneZero.Raycast(tempRay, out tempDistanceToZero);
		Vector3 tempPointAtZero = tempRay.GetPoint(tempDistanceToZero);
		//Debug.Log("Mouse-Position on z=0 > x: " + tempPointAtZero.x + ", y: " + tempPointAtZero.y + ", z: " + tempPointAtZero.z);
		// get point where the mouse hits object
		float tempDistanceToObject = 0f;
		tempPlaneObject.Raycast(tempRay, out tempDistanceToObject);
		Vector3 tempPointAtObject = tempRay.GetPoint(tempDistanceToObject);
		tempPointAtObject = tempPointAtObject - this.transform.position;
		//Debug.Log("Mouse-Position on object > x: " + tempPointAtObject.x + ", y: " + tempPointAtObject.y + ", z: " + tempPointAtObject.z);
		// and now:
		this.transform.position = tempPointAtZero - tempPointAtObject;

		// old version
		//this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, 0f);

		// as some sprites can be inactive at the start, we have to get all the sprites again
		mySprites = GetComponentsInChildren<SpriteRenderer>();

		if (this.tag == "Characters")
		{
			if (this.GetComponent<ICanEat>()){
				//Debug.Log("You have clicked on a character that can eat other characters.");
				changeBaseSpriteOrderTo(-10);
				changeBaseSpriteOrderOfItemInHand(-7);
			} else {
				changeBaseSpriteOrderTo(10);
				changeBaseSpriteOrderOfItemInHand(13);
			}
		}
		if (this.tag == "Items")
		{
			changeSortingLayer("Interactions");
			changeBaseSpriteOrderTo(3);
		}

		alignSprite();

		this.transform.localScale = Vector3.Scale(this.transform.localScale, scaleChange);

		dragPreviousPos = ScreenPosition2WorldSpace(Input.mousePosition);
	}

	void OnMouseDrag()
	{

		// Checking for active page (to be able to exclude dragging on the first and last page).

		if (myGameManager.GetComponent<MoveCamera>().currentCameraIndex == 0 || myGameManager.GetComponent<MoveCamera>().currentCameraIndex == myGameManager.GetComponent<MoveCamera>().cameraPositions.Length - 1.0)
		{
			//Debug.Log ("You have clicked on a draggable character on the first or last page.");
		} else 
		{
			//Debug.Log ("You have clicked on a dragable character NOT on the first or last page.")
			//Debug.Log("Dragging: " + this.transform.gameObject.name);

			// check, if the dragged object is still inside the page or not
			// as a preparation for:
			// don't allow for dragging outside the frame of the page
			// bool iAmInside = false;
			// if (myPagesCollisionBox.GetComponent<checkTriggerContent>().charactersHere.Contains(this.name) || myPagesCollisionBox.GetComponent<checkTriggerContent>().itemsHere.Contains(this.name))
			// {
			// 	Debug.Log (this.name + " is inside the page.");
			// 	iAmInside = true;
			// } else {
			// 	Debug.Log (this.name + " is NOT inside the page anymore.");
			// }

			// constrain mousePosition
			float border = 0.05f;
			Vector3 mousePos = Input.mousePosition;
			if (mousePos.x < (Screen.width * border))
			{
				mousePos.x = Screen.width * border;
			}
			if (mousePos.x > Screen.width - (Screen.width * border))
			{
				mousePos.x = Screen.width - (Screen.width * border);
			}
			if (mousePos.y < (Screen.height * border))
			{
				mousePos.y = Screen.height * border;
			}
			if (mousePos.y > Screen.height - (Screen.height * border))
			{
				mousePos.y = Screen.height - (Screen.height * border);
			}

			// change position
			//https://stackoverflow.com/questions/56435180/how-do-i-drag-objects-on-the-screen-using-onmousedrag
			Vector3 dragNowPosition = ScreenPosition2WorldSpace(mousePos);
			transform.Translate(dragNowPosition - dragPreviousPos);
			dragPreviousPos = dragNowPosition;

			// keeping the object on the main interaction line (though I don't really get, why I have to do this)
			this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, 0f);

			// don't allow for dragging below the ground
			if (this.tag == "Characters")
			{
				if (this.transform.position.y < 0)
				{
					this.transform.position = new Vector3(this.transform.position.x, 0f, this.transform.position.z);
				}
			}

			// change the facing direction of the object

			if (mySpriteCanChangeDirection){

				//Debug.Log ("Mouse X Movement: " + Input.GetAxis("Mouse X"));
				float mouseSensitivityTreshold = 0.03f;
				if (Input.GetAxis("Mouse X") > mouseSensitivityTreshold){
					//Debug.Log (" Mouse moving >>>>>");
					draggingDirectionIsToRight = true;
				}
				if (Input.GetAxis("Mouse X") < -mouseSensitivityTreshold){
					//Debug.Log (" Mouse moving <<<<<");
					draggingDirectionIsToRight = false;
				}

				alignSprite();
			}
		}
	}

	void OnMouseUp()
	{
		if (draggingDirectionIsToRight){
			this.transform.localScale = myStartScale;
		} else {
			this.transform.localScale = Vector3.Scale (myStartScale, new Vector3(-1f, 1f, 1f));
		}

		// put object back to ground after dragging
		alignSprite();
		this.transform.position = new Vector3(this.transform.position.x, 0f, this.transform.position.z);

		if (this.tag == "Characters")
		{
			changeSortingLayer("CharacterLayer");
			changeBaseSpriteOrderTo(0);
			changeBaseSpriteOrderOfItemInHand(3);
		}
		if (this.tag == "Items")
		{
			// check, if droped item is an real item, or an activeAnimal
			if (this.GetComponent<dropItem>() == null)
			{
				changeSortingLayer("CharacterLayer");
				changeBaseSpriteOrderTo(3);
			} else {
				changeBaseSpriteOrderTo(3);
			}
		}
	}

	void OnMouseExit()
	{
		//The mouse is no longer hovering over the GameObject so output this message each frame
		//Debug.Log("Mouse is no longer on GameObject.");
	}

	private Vector3 ScreenPosition2WorldSpace(Vector3 pos)
	{
	    Plane dragPlane = new Plane(Camera.main.transform.forward, transform.position);
	    Ray camRay = Camera.main.ScreenPointToRay(pos);
	    float enter = 0.0f;
	    if (dragPlane.Raycast(camRay, out enter))
	    {
	        return camRay.GetPoint(enter);
	    }

		return new Vector3 (0f,0f,0f);
	    //return dragPreviousPos;
	}

	public void changeSortingLayer(string _sortingLayerName)
	{
		if (this.mySprites != null)
		{
			for(int i = 0; i < this.mySprites.Length; i++){
				this.mySprites[i].sortingLayerName = _sortingLayerName;
			}
		} else {
			//Debug.Log ("No sprite found in: " + this.name);
		}
	}

	public void changeBaseSpriteOrderTo(int _newBaseSpriteOrder)
	{
		if (this.mySprites != null)
		{
			for(int i = 0; i < this.mySprites.Length; i++){
				int diff = (this.mySprites[i].sortingOrder - baseSpriteOrder);
				this.mySprites[i].sortingOrder = _newBaseSpriteOrder + diff;
			}
		}
		baseSpriteOrder = _newBaseSpriteOrder;
	}

	public void changeBaseSpriteOrderOfItemInHand(int _newBaseSpriteOrder)
	{
		if (GetComponent<itemHandling>().hasItem.Count > 0)
		{
			GameObject _itemInMyHand = GameObject.Find(GetComponent<itemHandling>().hasItem[0]);
			//Debug.Log ("And the item is: " + _itemInMyHand);
			_itemInMyHand.GetComponent<DragAndDropMe>().changeBaseSpriteOrderTo(_newBaseSpriteOrder);
		}
	}

	void updateItemPositionInHandOfCharacter(){
		if (this.tag == "Characters"){
			//Debug.Log("I'm a character!");

			if (GetComponent<itemHandling>().hasItem.Count > 0){
				GameObject _itemInMyHand = GameObject.Find(GetComponent<itemHandling>().hasItem[0]);
				//Debug.Log ("And I have the item: " + _itemInMyHand);
				this.GetComponent<itemHandling>().stickItemToArm();
			}
		}
	}

	void alignSprite(){
		if (draggingDirectionIsToRight){
			if (spriteFacingToRight){
				if (this.transform.localScale.x < 0){
					this.transform.localScale = new Vector3(-this.transform.localScale.x, this.transform.localScale.y, this.transform.localScale.z);
				}
			} else {
				if (this.transform.localScale.x > 0){
					this.transform.localScale = new Vector3(-this.transform.localScale.x, this.transform.localScale.y, this.transform.localScale.z);
				}
			}
		}
		if (!draggingDirectionIsToRight){
			if (spriteFacingToRight){
				if (this.transform.localScale.x > 0){
					this.transform.localScale = new Vector3(-this.transform.localScale.x, this.transform.localScale.y, this.transform.localScale.z);
				}
			} else {
				if (this.transform.localScale.x < 0){
					this.transform.localScale = new Vector3(-this.transform.localScale.x, this.transform.localScale.y, this.transform.localScale.z);
				}
			}
		}
	}
}
