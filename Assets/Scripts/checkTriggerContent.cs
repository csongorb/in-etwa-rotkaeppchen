﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class checkTriggerContent : MonoBehaviour
{
	internal List<string> charactersHere = new List<string>();
	internal List<string> itemsHere = new List<string>();

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

	void OnTriggerEnter(Collider collider)
	{
		GameObject collision = collider.gameObject.transform.parent.gameObject;

		//Debug.Log("Colliding with " + this.gameObject.name + " is: " + collision.gameObject.name);

		if (collision.gameObject.tag == "Characters")
		{
			if (!charactersHere.Contains(collision.transform.name))
			{
				charactersHere.Add(collision.transform.name);
			}
		}
		if (collision.gameObject.tag == "Items")
		{
			if (!itemsHere.Contains(collision.transform.name))
			{
				itemsHere.Add(collision.transform.name);
			}
		}
	}

	void OnTriggerExit(Collider collider)
	{
		GameObject collision = collider.gameObject.transform.parent.gameObject;

		//Debug.Log("Not colliding with " + this.gameObject.name + " anymore is: " + collision.gameObject.name);

		if (collision.gameObject.tag == "Characters")
		{
			charactersHere.Remove(collision.transform.name);
		}
		if (collision.gameObject.tag == "Items")
		{
			itemsHere.Remove(collision.transform.name);
		}
	}
}
