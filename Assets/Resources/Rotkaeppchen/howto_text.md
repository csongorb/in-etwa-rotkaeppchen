# How-To Write Texts?

## functionalities good to know

- all texts on a page, that are `true` will be displayed
	- in their order in the json-file
- a text on page A can be depending characters (and other conditions) on page B
- add `none` as the first entry into `charactersEatenByName` to display a text only when the character has eaten no other character
- add `anybody` as the first entry into `charactersEatenByName` to display a text only when the character at least one other character

## an example

```
		{
			"myNecessaryCharactersOnAPage": [
				{
					"nameOfThePage": "House",
					"necessaryCharactersByName": [
						"Wolf"
					],
					"otherCharactersAllowed": true
				}
			],
			"myNecessaryItemsWithCharacters": [],
			"myNecessaryCharactersEaten": [
				{
                    "characterNameWhoCanEat": "Wolf",
					"charactersEatenByName": [
						"Rotkaeppchen",
						"Mother"
					]
                }
			],
			"pageText": "Der Wolf war von Rotkäppchen und der Mutter schon ziemlich satt."
		},
```
