## UI

173563__bratok999__page-turning.wav
https://freesound.org/people/Bratok999/sounds/173563/

## Nature

278888__theshaggyfreak__shaking-branch-2
https://freesound.org/people/theshaggyfreak/sounds/278888/

442490__bonnyorbit__birds-in-trees
https://freesound.org/people/BonnyOrbit/sounds/442490/

437356__giddster__rustling-leaves
https://freesound.org/people/giddster/sounds/437356/

### InspectorJ

- https://freesound.org/people/InspectorJ/sounds/418262/
- https://freesound.org/people/InspectorJ/sounds/345687/
- https://freesound.org/people/InspectorJ/sounds/416529/
- https://freesound.org/people/InspectorJ/sounds/456440/

This sounds are NOT in the public domain. You MUST attribute/credit the sound if you use it. If you would prefer to not have to give attribution, then take a look at the attribution-free license.

An example of how you might credit is by putting this in the description/credits:
"title.wav" by InspectorJ (www.jshaw.co.uk) of Freesound.org
