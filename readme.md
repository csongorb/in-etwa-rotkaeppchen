# In etwa Rotkaeppchen

A narrative playground by Cosima (8 - 12), Dalma (11 - 14), and [Csongor (44 - 47)](http://www.csongorb.com) Baranyai. We hope you have as much fun messing around with Rotkaeppchen as we did.
For now only available in German.

- [*In etwa Rotkäppchen* on itch.io](https://csongorb.itch.io/inetwarotkaeppchen)

Where it fits, text is based on the fairytale as written by the Brothers Grimm.

Made with:

- Unity 2022.3.22f1

More info:

- [changelog](changelog.md)
- [process journal](Journal/journal.md)
- [privacy policy](PrivacyPolicy_InEtwaRotkaeppchen.html)

## Instructions

- Left & Right Arrow OR mouse OR touch: goto last/next page (move camera between positions)
- mouse OR touch: drag & drop characters

## Level Design / Modding / How to change the game?

- do whatever you want
- just take a look at the existing story and try to rebuild it
	- you need a Game Manager in each game to be able to turn pages
	- you need a Page Manager on each page to handle everything

## Additional Credits

Additional Art:

- Hanna Borbola

Voice Acting:

- Dalma
- Cosima
- Gizi
- Tine
- Csongor

Additional Sounds:

- see Git repository

Thanks / Feedback / Playtesting / Inspiration:

- students from the class "Build the toy (first)"
	- Hanna Borbola
	- Sam Luckhardt
	- Paul Kettmann
- students from the class "P2/ Narratives"
- Sebastian Stamm
- Lena Falkenhagen

## License

[![Creative Commons License](https://i.creativecommons.org/l/by-nc/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc/4.0/)
This work is licensed under a [Creative Commons Attribution-NonCommercial 4.0 International License](http://creativecommons.org/licenses/by-nc/4.0/).
