# Todos / Features / Backlog

- add some juice (or better: let's procrastinate some juice to this, this should work well with Dalma and Cosima)
	- add sounds
	- add page texture overlay
- Wolf should take the objects from the characters he is eating
- add small camera movement / rotations when playing on mobile (gyroscope)
- closable door is very glitchy
	- it flickers
	- character can glitch through the obstacle

## Obsolete ideas

Still keeping them for documentation purposes.

- add some additional small wimmelbild-like interactions
	- physic based, apples falling down, etc.
- add some special interactions (as systemic as possible)
	- the hunter can...
	- grandmother can just walk slower (and maybe also the Wolf, if he has eaten?)
- add some visual elements in the areas where the frames are overlapping
	- emphasize the fact, that they don't have to be dragged outside to reach the other page
	- maybe the characters themselves could do something to show, that they are in the overlapping area
		- like looking at the other direction?
- let the player drop objects on the ground AROUND the characters
- add more depth to the images, use more the 3d space in a more creative way
	- should build up the objects more like paper crafted
	- maybe I need somehow more camera movement to show that the scene is 3d?
		- dragging something also moves the camera?
		- turning the pages moves the camera?
- having the main interactions happening on a plane is an obvious and logical idea
	- but maybe I need an line (where the characters are moving along) to be more flexible
		- like Grandmother having to move into the bad, but the other characters in front of
- add some additional languages (de/en/hu)
- add some talking sound when characters are overlapping with each other

# Version ?

# Version 0.2 - Coming together

- support for wide aspect ratios
	- between 4:3 and 19.5:9
- ignoring clicks on objects bellow UI elements
- updated Privacy Polity
- new and more flexible icons for Android

# Version 0.1.10.1 - The Android API 34 Update (18.8.2024)

- updated the minimum (not just the target) API level to 34

# Version 0.1.10 - The Final Countdown (14.8.2024)

- update to Android 14 (API level 34)
- text update
	- added new & adjusted old texts
	- adjustments to text conditions
- scene update
	- added new background objects
	- adjusted objects
- sound adjustments
	- deleted page-flipping sound
	- added some new sounds

# Version 0.1.9 - The Fast Update (21.3.2024)

- update to Unity 2022.3.
- update to Andoid 13 (API level 33)

# Version 0.1.8 - The Eclectic Play Store Test (18.2.2021)

- characters can now turn their faces to the direction they are dragged at
- lot of small sprite layer improvements
	- Wolf is now behind the characters he wants to eat, etc.
	- Animals are now behaving better
- added front-door to Grandmothers house
	- openable from the inside
	- not letting characters and objects pass
- lot of new objects in the scene
	- rotating sun, additional bushes, fence
- finally: new sprite for Hunter
- added new text condition
	- can now display texts depending on characters eaten
- general features
	- added new logo
- Android version specifics
	- added some Play Store images
	- generated project sign key through Unity (custom keystore)
		- not signed with debug key anymore
		- decided to not sign it through Google Play directly
			- as it would not allow for itch.io, if I get it correctly (?)
	- target API level is 29; API levels 19+
	- now building for 32- and 64-bit target architectures
- testing different mouse-movement camera behaviour for different platforms
	- standalone: moving the mouse also moves the camera a little
	- Android: no camera movement
- bugs solved
	- player can't turn the pages with keys at the same time as dragging the characters around

# Version 0.1.7 (23.11.2020)

- added some Item-Containers (in real disguised Characters, without the possibility to move)
	- Nail on the wall (at Grandmother's house)
	- Soil on the ground (Meadows)
	- incl. the corresponding logic
		- items can be grabbed now also, when they are in the back- / foreground
	- very buggy / temporary implementation though
- more Rotkaeppchen interactions possible now
	- Flowers can be taken
	- Scissors are not at Grandmother's house
- more additional interactions added
	- Vases
	- Cat and Bird
- overhaul of the sprite-rendering-layer system
	- interactions happening now visibly in the foreground
	- items attached to object is the background are now visibly in the background
	- etc.
- images are now (like cardboard / paper crafted objects) rooted in 3d space
	- added small camera movement to make this 3d-effect more visible
- added smooth camera transitions
- more animations
	- Wolf can now also attack
- new sounds
	- all characters do now have some sound
	- also a lot of additional sounds
- huge scene overhaul
	- added a lot of new elements & interactions
	- added a lot of new sounds, basically almost to each object

# Version 0.1.6 (10.10.2020)

- added
	- Wolf can now
		- eat up to two characters (with audio)
	 	- release last eaten character (incl. item)
	- Scissors
	- the ability for objects to cut open characters, who have eaten other characters
	- some new audio for Wolf, Rotkaeppchen, and Grandmother

# Version 0.1.5.1 (9.6.2020)

- added mod by Paul Kettmann
	- (not working together with the other two mods)
	- added ufo interactions

# Version 0.1.5 (9.6.2020)

- added mod by Hanna Borbola
	- some awesome new sprites
		- environment: trees, bushes, well
		- characters: Samara, bird
	- new location: Deep Forest
	- new interactions, ending with Samara getting out of the well
- added mod by Sam Luckhardt
	- some new elements:
		- carpet beater ("Klopfer"), golden key, dirty pillow, key hole
	- some new interactions
		- starting with Rotkaeppchen having to clean the dirty pillow with the pillow beater
		- attaining the golden key
		- opening the secret door in the tree in the forest

# Version 0.1.4.1 (31.5.2020)

- adjusted
	- there can be now more "true" text-dependency checks
		- each adding a new part to the visible text

# Version 0.1.4 - The Animations & Text-Dependency Update (25.5.2020)

- restructuring
	- text-dependency system overhaul
		- texts can be depending now on
			- characters on the page itself
			- characters on other pages
			- items in characters hands
		- much more flexible!
- added
	- animations for some characters (Grandmother and Hunter)

# Version 0.1.3 - The Items & Art Update (17.5.2020)

- added
	- random page turning sounds (is this now too much like a book?)
	- tree shaking sounds
	- lot of new sprites and small interactions
	- item logic, incl. some important items
- restructuring
	- changed from collision-detection to trigger-detection for checking characters and items inside a trigger area (on each page, etc.)
		- now I could add some physic based objects, yeah!
		- why have I not done this earlier?

# Version 0.1.2 (11.5.2020)

- updated
	- some new and additional texts
- added
	- (mandatory) default texts for arbitrary characters at each location
	- fixed horizontal field of view for different aspect ratios
	- finally: some juicy interactions: clicking on objects makes them bigger
- restructuring
	- locations as "main" objects / elements
	- page-texts now checking for character-names as strings
	- finally: loads all text-date from json-files
	- drag&drop now a script on the draggable objects themselves

# Version 0.1.1 (5.5.2020)

- full setup of the main elements of the fairytale
	- all locations
	- all characters
	- (still no items and special interactions)
- added lot of texts (inspired by the Brothers Grimm)
- restructured scene
	- each page is a self-contained GameObject
	- each character is self-contained GameObject

# Version 0.1 (28.4.2020)

- fixed aspect ration (16:9)
- half fancy test scenery
	- incl. three pages already
	- some drawn characters
- text depending on characters on page
	- some fairytale-like placeholder text
- added first interactions
	- can move cameras between positions with the arrow keys or mouse
	- can drag & drop characters (tag: DragAndDrop)
